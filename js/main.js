function loadTemplate() {
	templateTags();
};

/* ==========================================================================
 Template data array
 ========================================================================== */
function templateTags() {
	var headTag = '<header></header>';
	var contentTag = '<div id="contentWrap"></div>'
	var footerTag = '<footer></footer>'
	var templateArray = [headTag, contentTag, footerTag];
	for ( i = 0; i < templateArray.length; i++) {
		$('#dataWrap').append(templateArray[i]);
	}

	loadHeader();
};

/* ==========================================================================
 Header
 ========================================================================== */
function loadHeader() {
	var logoData = "Test Site";
	var mainNavArray = ['Link 1', 'Link 2', 'Link 3', 'Link 4'];

	$('header').append('<h1 id="logo"></h1>');
	$('#logo').append('<a href="javascript:void(0)">' + logoData + '</a>');
	$('header').append('<ul id="hNav"></ul>');

	for ( i = 0; i < mainNavArray.length; i++) {
		$('#hNav').append('<li><a href="javascript:void(0)">' + mainNavArray[i] + '</a></li>');
	}
	loadContent();
};

/* ==========================================================================
 Content
 ========================================================================== */

function loadContent() {
	function loadContentFunctions() {
		loadHero();
		loadModal();
		loadModule();
		setModuleHeight();
	};

	function loadHero() {
		var heroContent = 'Well, the way they make shows is, they make one show. <br />That show\'s called a pilot.';
		$('#contentWrap').append('<div id="hero">' + heroContent + '</div>');
	};

	function loadModal() {
		$('<div id="modalLinkWrap"><a href="#openModal">Open Modal</a></div><div id="openModal" class="modalDialog"></div>').insertAfter('#hero');
		$('#openModal').append('<div><a href="#close" title="Close" class="close">X</a><h2>This is a modal box</h2><p>You think water moves fast? You should see ice.</p><p>It moves like it has a mind. Like it knows it killed the world once and got a taste for murder.</p></div>');
	};

	function loadModule() {
		var moduleContent1 = '<h2 class="dropCap">1</h2><p>Then they show that show to the people who make shows, and on the strength of that one show they decide if they\'re going to make more shows.</p>'
		var moduleContent2 = '<h2 class="dropCap">2</h2><p>Some pilots get picked and become television programs.</p>'
		var moduleContent3 = '<h2 class="dropCap">3</h2><p>Some don\'t, become nothing. She starred in one of the ones that became nothing.</p>'
		var moduleArrayData = [moduleContent1, moduleContent2, moduleContent3];
		$('#contentWrap').append('<ul id="moduleData"></ul>');
		for ( i = 0; i < moduleArrayData.length; i++) {
			$('#moduleData').append("<li>" + moduleArrayData[i] + "</li>");
		}
	};

	function setModuleHeight() {
		var maxHeight = -1;
		$('#moduleData li').each(function() {
			maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
		});

		$('#moduleData li').each(function() {
			$(this).height(maxHeight);
		});
	};

	loadContentFunctions();
	loadFooter();
};

/* ==========================================================================
 Footer
 ========================================================================== */
function loadFooter() {
	var currentYear = new Date().getFullYear();
	var originYear = "2006";
	var footerData = "&copy;&nbsp;" + originYear + "&nbsp;-&nbsp;" + currentYear;

	$('footer').append(footerData);
};
